class CreateProposals < ActiveRecord::Migration[6.1]
  def change
    create_table :proposals do |t|
      t.belongs_to :user, null: false, foreign_key: true, index: true
      t.belongs_to :proposal_type, polymorphic: true, null: false, index: true
      t.string :description, null: false

      t.timestamps
    end
  end
end
