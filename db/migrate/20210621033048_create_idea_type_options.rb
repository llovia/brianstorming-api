class CreateIdeaTypeOptions < ActiveRecord::Migration[6.1]
  def change
    create_table :idea_type_options do |t|
      t.belongs_to :idea_proposal_type, null: false, foreign_key: true
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
