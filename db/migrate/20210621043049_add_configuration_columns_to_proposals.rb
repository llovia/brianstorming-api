class AddConfigurationColumnsToProposals < ActiveRecord::Migration[6.1]
  def change
    add_columns :proposals, :multiple_options, :accessible_by_link, :registered_users_can_interact, :allows_anonymous_answers, :users_can_add_answers, :users_can_view_results, :has_deadline, :user_can_comment, :user_can_vote , type: :boolean, default: false
    add_column :proposals, :deadline, :date
  end
end
