class Proposal < ApplicationRecord
  belongs_to :user
  belongs_to :proposal_type, polymorphic: true, dependent: :destroy

  validates :user, :proposal_type, :description, presence: true
end
