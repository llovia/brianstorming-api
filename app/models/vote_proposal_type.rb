class VoteProposalType < ApplicationRecord
  has_one :proposal, as: :proposal_type
  has_many :options, class_name: 'VoteTypeOption', dependent: :destroy
end
