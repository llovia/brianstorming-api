require "test_helper"

class ProposalsByTypeReportTest < ActiveSupport::TestCase
  setup do
    @report = Report.new
  end

  test "should return correct message if proposals of all types exists" do
    assert_equal report_result, @report.proposals_by_type
    assert_equal reported_proposal_types_count, proposals.count
  end

  test "should return message only for the proposal types of existant proposals" do
    assert_difference 'reported_proposal_types_count', -1 do
      proposals.first.destroy
    end
    assert_equal report_result, @report.proposals_by_type
  end

  test "should return no proposals message if there are no proposals of any type" do
    Proposal.destroy_all
    assert_equal I18n.t('reports.proposals_by_type.no_proposals_message'), @report.proposals_by_type
  end

  private

  def report_result
    Proposal.includes(:proposal_type).collect(&:proposal_type).collect do |proposal_type|
      "#{proposal_type.model_name.human}: #{proposals.select { |proposal| proposal.proposal_type.instance_of?(proposal_type.class) }.count}"
    end.join(', ')
  end

  def reported_proposal_types_count
    report_result.split(',').count
  end
end
