class MeetingTypeOption < ApplicationRecord
  belongs_to :meeting_proposal_type
  has_many :votes, as: :votable, dependent: :destroy

  validates :start_date, presence: true
end
