class CreateComments < ActiveRecord::Migration[6.1]
  def change
    create_table :comments do |t|
      t.belongs_to :idea_type_option, null: false, foreign_key: true
      t.belongs_to :user, null: false, foreign_key: true
      t.text :description, null: false
      t.date :date, null: false

      t.timestamps
    end
  end
end
