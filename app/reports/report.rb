class Report
  def proposals_by_type
    return I18n.t('reports.proposals_by_type.no_proposals_message') unless Proposal.any?

    Proposal.includes(:proposal_type).group_by(&:proposal_type).collect do |proposal_type, proposals|
      "#{proposal_type.model_name.human}: #{proposals.count}"
    end.join(', ')
  end

  def user_with_more_ideas
    ideas = Proposal.preload(:proposal_type).where(proposal_type_type: 'IdeaProposalType')
    ideas.group(:user).count.select { |_k, v| v == ideas.group(:user).count.values.max }.keys.first
  end

  def users_with_votes_on_all_proposals
    votes_grouped_by_type = Vote.includes(:user).where(votable_type: ['IdeaTypeOption', 'VoteTypeOption', 'MeetingTypeOption']).group_by(&:votable_type)
    users = votes_grouped_by_type.collect { |k, v| { k => v.collect(&:user).uniq } }.collect(&:values).flatten
    users.select { |user| users.count(user) == 3 }.uniq
  end

  def users_with_votes_and_no_comments
    result = User.includes(:votes, :comments).select { |user| user.votes.any? && user.comments.empty? }
    result.any? ? result : I18n.t('reports.users_with_votes_and_no_comments.no_users_with_votes_and_no_comments_message')
  end
end
