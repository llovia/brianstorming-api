class IdeaTypeOption < ApplicationRecord
  belongs_to :idea_proposal_type
  has_many :votes, as: :votable, dependent: :destroy
  has_many :comments, dependent: :destroy

  validates :title, presence: true
end
