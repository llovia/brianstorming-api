class CreateMeetingTypeOptions < ActiveRecord::Migration[6.1]
  def change
    create_table :meeting_type_options do |t|
      t.belongs_to :meeting_proposal_type, null: false, foreign_key: true
      t.datetime :start_date, null: false
      t.datetime :end_date
      t.boolean :all_day

      t.timestamps
    end
  end
end
