class MeetingProposalType < ApplicationRecord
  has_one :proposal, as: :proposal_type
  has_many :options, class_name: 'MeetingTypeOption', dependent: :destroy
end
