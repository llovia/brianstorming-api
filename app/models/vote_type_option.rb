class VoteTypeOption < ApplicationRecord
  belongs_to :vote_proposal_type
  has_many :votes, as: :votable

  validates :description, presence: true
end
