class Comment < ApplicationRecord
  belongs_to :idea_type_option
  belongs_to :user
  has_many :votes, as: :votable, dependent: :destroy

  validates :user, :idea_type_option, :description, :date, presence: true
end
