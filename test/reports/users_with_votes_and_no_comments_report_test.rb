require "test_helper"

class UsersWithVotesAndNoCommentsReportTest < ActiveSupport::TestCase
  setup do
    @report = Report.new
  end

  test "should return message if there are no users" do
    User.destroy_all
    assert_equal I18n.t('reports.users_with_votes_and_no_comments.no_users_with_votes_and_no_comments_message'),
                 @report.users_with_votes_and_no_comments
  end

  test "should return message if no users have votes" do
    Vote.destroy_all
    assert_equal I18n.t('reports.users_with_votes_and_no_comments.no_users_with_votes_and_no_comments_message'),
                 @report.users_with_votes_and_no_comments
  end

  test "should return message if all users have comments" do
    User.includes(:comments).select { |user| user.comments.empty? }.each do |user|
      Comment.create!(
        idea_type_option: idea_type_options(:one),
        user: user,
        description: 'Lorem Ipsum',
        date: Date.today
      )
    end
    assert_equal I18n.t('reports.users_with_votes_and_no_comments.no_users_with_votes_and_no_comments_message'),
                 @report.users_with_votes_and_no_comments
  end

  test "should return array of users if any have votes but no comments" do
    assert_instance_of Array, @report.users_with_votes_and_no_comments
    assert_not_empty @report.users_with_votes_and_no_comments
    assert_equal votes.collect(&:user).select { |user| user.comments.empty? }, @report.users_with_votes_and_no_comments
  end
end