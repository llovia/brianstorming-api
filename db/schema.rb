# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_06_21_043049) do

  create_table "comments", force: :cascade do |t|
    t.integer "idea_type_option_id", null: false
    t.integer "user_id", null: false
    t.text "description", null: false
    t.date "date", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["idea_type_option_id"], name: "index_comments_on_idea_type_option_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "idea_proposal_types", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "idea_type_options", force: :cascade do |t|
    t.integer "idea_proposal_type_id", null: false
    t.string "title"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["idea_proposal_type_id"], name: "index_idea_type_options_on_idea_proposal_type_id"
  end

  create_table "meeting_proposal_types", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "meeting_type_options", force: :cascade do |t|
    t.integer "meeting_proposal_type_id", null: false
    t.datetime "start_date", null: false
    t.datetime "end_date"
    t.boolean "all_day"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["meeting_proposal_type_id"], name: "index_meeting_type_options_on_meeting_proposal_type_id"
  end

  create_table "proposals", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "proposal_type_type", null: false
    t.integer "proposal_type_id", null: false
    t.string "description", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "multiple_options", default: false
    t.boolean "accessible_by_link", default: false
    t.boolean "registered_users_can_interact", default: false
    t.boolean "allows_anonymous_answers", default: false
    t.boolean "users_can_add_answers", default: false
    t.boolean "users_can_view_results", default: false
    t.boolean "has_deadline", default: false
    t.boolean "user_can_comment", default: false
    t.boolean "user_can_vote", default: false
    t.date "deadline"
    t.index ["proposal_type_type", "proposal_type_id"], name: "index_proposals_on_proposal_type"
    t.index ["user_id"], name: "index_proposals_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "vote_proposal_types", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "vote_type_options", force: :cascade do |t|
    t.integer "vote_proposal_type_id", null: false
    t.string "description", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["vote_proposal_type_id"], name: "index_vote_type_options_on_vote_proposal_type_id"
  end

  create_table "votes", force: :cascade do |t|
    t.string "votable_type", null: false
    t.integer "votable_id", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_votes_on_user_id"
    t.index ["votable_type", "votable_id"], name: "index_votes_on_votable"
  end

  add_foreign_key "comments", "idea_type_options"
  add_foreign_key "comments", "users"
  add_foreign_key "idea_type_options", "idea_proposal_types"
  add_foreign_key "meeting_type_options", "meeting_proposal_types"
  add_foreign_key "proposals", "users"
  add_foreign_key "vote_type_options", "vote_proposal_types"
  add_foreign_key "votes", "users"
end
