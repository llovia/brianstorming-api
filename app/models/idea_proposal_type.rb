class IdeaProposalType < ApplicationRecord
  has_one :proposal, as: :proposal_type
  has_many :options, class_name: 'IdeaTypeOption', dependent: :destroy
end
