class CreateVoteTypeOptions < ActiveRecord::Migration[6.1]
  def change
    create_table :vote_type_options do |t|
      t.belongs_to :vote_proposal_type, null: false, foreign_key: true
      t.string :description, null: false

      t.timestamps
    end
  end
end
